#include <iostream>
#include <string>
#include <regex>
#include <cxxopts.hpp>
#include "polymorphictaskqueue.hpp"
#include "filecrcsignaturecreator.hpp"

constexpr int64_t maxBlockSize = 1024 * 1024 * 1024; // 1 GB

// extract block size by regular expression
int64_t blockSizeFromString(const std::string& s)
{
  if (s.empty())
    throw std::runtime_error("Empty block size provided");
  std::unordered_map<char, int64_t> multipliers {
    {'b', 1},
    {'k', 1024},
    {'m', 1024 * 1024}
  };
  // regex for extracting block size
  std::regex re(R"(^(\d+)([bB]|[kK]|[mM])?$)");
  std::smatch match;
  if (!std::regex_match(s.begin(), s.end(), match, re))
    throw std::runtime_error("Wrong format of block size provided. Examples of correct format: 1024, 1024B, 1K, 1024K, 1M, 1024M");
  int64_t num = std::stoll(match[1].str());
  std::string multiplier = match[2].str();
  int64_t result = num * multipliers[multiplier.empty() ? 'b' : std::tolower(multiplier[0])];
  if (result < num || result > maxBlockSize)
    throw std::runtime_error("Block size is too large");
  return result;
}

int main(int argc, char* argv[]) {
  auto t1 = std::chrono::high_resolution_clock::now();
  cxxopts::Options options("fcrcsign", "Utility for a file signature creation");
  options.allow_unrecognised_options().show_positional_help().add_options()(
    "h,help", "Show this help")(
    "s,source", "Source file which signature will be created", cxxopts::value<std::string>())(
    "o,out", "Output signature file", cxxopts::value<std::string>())(
    "b,block-size", "Block size for each crc32 sum", cxxopts::value<std::string>()->default_value("1M"))(
    "t,time", "Show utility execution time");
  cxxopts::ParseResult result;
  try {
    result = options.parse(argc, argv);
  }  catch (std::exception& e) {
    std::cout << e.what() << std::endl;
    std::cout << options.help() << std::endl;
    return 1;
  }
  if (result["help"].count() > 0) {
    std::cout << options.help() << std::endl;
    return 1;
  }
  if (result.unmatched().size() > 0) {
    std::cout << "Unknown option: " << result.unmatched()[0] << std::endl;
    std::cout << options.help() << std::endl;
    return 1;
  }
  if (result["s"].count() == 0) {
    std::cout << "Error: source file must be provided" << std::endl;
    std::cout << options.help() << std::endl;
    return 1;
  }
  if (result["o"].count() == 0) {
    std::cout << "Error: out file must be provided" << std::endl;
    std::cout << options.help() << std::endl;
    return 1;
  }
  bool printExecutionTime = (result["t"].count() > 0);
  int64_t blockSize;
  try {
    blockSize = blockSizeFromString(result["b"].as<std::string>());
  }  catch (std::exception& e) {
    std::cout << "Error: " << e.what() << std::endl;
    std::cout << options.help() << std::endl;
    return 1;
  }
  FileCrcSignatureCreator<> f{};
  try {
    f.setInputFile(result["s"].as<std::string>())
      .setOutputFile(result["o"].as<std::string>())
      .setBlockSize(blockSize)
      .initExecutor(std::thread::hardware_concurrency())
      .process();
  }  catch (std::exception& e) {
    std::cout << "Error: " << e.what() << std::endl;
  }
  if (printExecutionTime) {
    auto t2 = std::chrono::high_resolution_clock::now();
    std::cout << "Execution time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " ms" << std::endl;
  }
  return 0;
}
