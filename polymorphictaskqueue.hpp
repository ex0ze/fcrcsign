#pragma once
#include <algorithm>
#include <tuple>
#include <thread>
#include <deque>
#include <vector>
#include <mutex>
#include <future>
#include <condition_variable>

namespace processing {
namespace impl {

template <bool KeepResult>
class TaskQueueImpl
{
  enum class ThreadStatus {
    Free,
    Busy
  };

public:
  TaskQueueImpl(std::size_t numWorkers = std::thread::hardware_concurrency()) : mNumWorkers(numWorkers)
  {
    mRunning.store(false);

  }
  TaskQueueImpl(const TaskQueueImpl&) = delete;
  TaskQueueImpl& operator=(const TaskQueueImpl&) = delete;
  TaskQueueImpl(TaskQueueImpl&&) = default;
  TaskQueueImpl& operator=(TaskQueueImpl&&) = default;
  void stop()
  {
    if (!mRunning.load())
      return;
    mRunning.store(false);
    mNotifier.notify_all();
    for (auto& w : mWorkers)
      w.join();
    mWorkers.clear();
  }
  bool start()
  {
    if (mRunning.load())
      return false;
    mRunning.store(true);
    initWorkers(mNumWorkers);
    return true;
  }
  template <typename Func, typename ... Args>
  auto pushTask(Func && f, Args&&...args)
  {
    using Ret = std::invoke_result_t<Func, Args...>;
    if constexpr (KeepResult == true) {
      auto res_promise { std::make_shared<std::promise<Ret>>() };
      auto res_future { res_promise->get_future() };
      std::unique_lock<decltype (mLocker)> locker(mLocker);

      mTasks.emplace_back([fn = std::forward<Func>(f), tp = std::make_tuple(std::forward<Args>(args)...), promise = std::move(res_promise)]() mutable {
        if constexpr (!std::is_void_v<Ret>) //! workaround for void - non void functions
          promise->set_value(std::apply(std::forward<Func>(fn), std::forward<decltype (tp)>(tp)));
        else {
          std::apply(std::forward<Func>(fn), std::forward<decltype (tp)>(tp));
          promise->set_value();
        }
      });
      locker.unlock();
      mNotifier.notify_one();
      return res_future;
    } else {
      std::unique_lock<decltype (mLocker)> locker(mLocker);
      mTasks.emplace_back([fn = std::forward<Func>(f), tp = std::make_tuple(std::forward<Args>(args)...)] () mutable {
        std::apply(std::forward<Func>(fn), std::forward<decltype (tp)>(tp));
      });
      locker.unlock();
      mNotifier.notify_one();
    }
  }
  void waitForCompletion(std::chrono::milliseconds ms)
  {
    while (!(mTasks.empty() && std::count(mWorkersStatus.begin(), mWorkersStatus.end(), ThreadStatus::Busy) == 0)) {
      std::this_thread::sleep_for(ms);
    }
  }
  ~TaskQueueImpl()
  {
    stop();
  }
private:

  void initWorkers(std::size_t num)
  {
    mWorkersStatus.resize(num, ThreadStatus::Free);
    for (std::size_t i {0}; i < num; ++i)
      mWorkers.emplace_back([this, i](){
        while (mRunning.load()) {
          std::unique_lock<decltype (mLocker)> locker(mLocker);
          mNotifier.wait(locker, [this]{return !mTasks.empty() || !mRunning.load();});
          if (!mRunning.load())
            return ;
          auto task = std::move(mTasks.front());
          mTasks.pop_front();
          mWorkersStatus[i] = ThreadStatus::Busy;
          locker.unlock();
          task();
          mWorkersStatus[i] = ThreadStatus::Free;
        }
      });
  }

  std::vector<std::thread>                mWorkers;
  std::vector<ThreadStatus>               mWorkersStatus;
  std::deque<std::function<void(void)>>   mTasks;
  std::mutex                              mLocker;
  std::condition_variable                 mNotifier;
  std::atomic<bool>                       mRunning;
  std::size_t                             mNumWorkers;
};

}

using KeepResultTaskQueue = impl::TaskQueueImpl<true>;
using DiscardResultTaskQueue = impl::TaskQueueImpl<false>;
}
