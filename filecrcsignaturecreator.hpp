#pragma once
#include <string>
#include <string_view>
#include <fstream>
#include <filesystem>
#include <memory>
#include "crc32.hpp"
#include "polymorphictaskqueue.hpp"

// driver class with customization of Hasher and Executor types
// hasher must define Hasher::hash_type and member function Hasher::hash_type Hasher::hash(std::string_view buf);
// executor must define start() and pushTask(Func, Args && ...) member functions
template <typename Hasher = util::Crc32Hasher, typename Executor = processing::DiscardResultTaskQueue>
class FileCrcSignatureCreator
{
public:
  explicit FileCrcSignatureCreator(Hasher hasher = {}) : mHasher(std::move(hasher))
  {
    static_assert (std::is_integral_v<typename Hasher::hash_type>, "Error. Hasher::hash_type must be trivial type");
  }
  //!
  //! \brief setInputFile open input file
  //! \param file file to open
  //! \return this
  //!
  FileCrcSignatureCreator& setInputFile(std::string_view file)
  {
    namespace fs = std::filesystem;
    std::filesystem::path path(file);
    if (!std::filesystem::exists(path))
      throw std::runtime_error("File " + std::string(file) + " does not exist");
    auto perms = fs::status(path).permissions();
    if (fs::perms::none == (perms & fs::perms::owner_write))
      throw std::runtime_error("File" + std::string(file) + " is not writable");
    pInputFile.reset(new std::ifstream(path, std::ios::binary));
    mFileSize = fs::file_size(path);
    return *this;
  }
  //!
  //! \brief setOutputFile create and open output file
  //! \param file file to open
  //! \return this
  //!
  FileCrcSignatureCreator& setOutputFile(std::string_view file)
  {
    namespace fs = std::filesystem;
    std::filesystem::path path(file);
    pOutputFile.reset(new std::ofstream(path, std::ios::binary));
    auto perms = fs::status(path).permissions();
    if (fs::perms::none == (perms & fs::perms::owner_write))
      throw std::runtime_error("File" + std::string(file) + " is not writable");
    return *this;
  }
  template <typename ... Args>
  //!
  //! \brief initExecutor create and initialize executor context
  //! \param args arguments that will be forwarded to the executor
  //! \return this
  //!
  FileCrcSignatureCreator& initExecutor(Args && ... args)
  {
    pExecutor.reset(new Executor(std::forward<Args>(args)...));
    // member function start() must be defined by the executor
    pExecutor->start();
    return *this;
  }
  //!
  //! \brief setBlockSize
  //! \param blockSize block size for computing hashes
  //! \return this
  //!
  FileCrcSignatureCreator& setBlockSize(std::size_t blockSize) noexcept
  {
    mBlockSize = blockSize;
    return *this;
  }
  //!
  //! \brief process driver function
  //!
  void process()
  {
    using namespace std::chrono_literals;
    if (!pInputFile)
      throw std::runtime_error("Input file not opened");
    if (!pOutputFile)
      throw std::runtime_error("Output file not opened");
    if (mBlockSize == 0)
      throw std::runtime_error("Block size must be greater than 0");
    if (!pExecutor)
      initExecutor();
    std::size_t parts = mFileSize / mBlockSize;
    if (parts * mBlockSize < mFileSize)
      ++parts;
    std::vector<typename Hasher::hash_type> hashes;
    hashes.resize(parts);
    std::string currPartContents;
    for (std::size_t currPart = 0; currPart < parts; ++currPart) {
      bool isLastPart = (currPart == parts - 1);
      std::size_t currPartSize = isLastPart
                                   ? mFileSize - (currPart * mBlockSize)
                                   : mBlockSize;
      currPartContents.resize(mBlockSize, 0);
      pInputFile->read(currPartContents.data(), currPartSize);
      // push hashing task to the executor
      // hash computing is much faster than reading file from disk,
      // therefore the memory won't overflow
      pExecutor->pushTask([idx = currPart, contents = std::move(currPartContents), &hashes, this] {
        hashes[idx] = mHasher.hash(contents);
      });
    }
    // wait for all tasks done
    pExecutor->waitForCompletion(10ms);
    pOutputFile->write(reinterpret_cast<const char*>(hashes.data()), hashes.size() * sizeof(typename Hasher::hash_type));
  }
  constexpr const Hasher& getHasher() const noexcept { return mHasher; }
  constexpr Hasher& getHasher() noexcept { return mHasher; }
  constexpr const Executor * getExecutor() const noexcept { return pExecutor.get(); }
  constexpr Executor * getExecutor() noexcept { return pExecutor.get(); }
private:
  std::unique_ptr<std::ifstream> pInputFile;
  std::unique_ptr<std::ofstream> pOutputFile;
  std::unique_ptr<Executor> pExecutor;
  Hasher mHasher;
  int64_t mBlockSize = 1024 * 1024;
  std::size_t mFileSize = 0;
};

