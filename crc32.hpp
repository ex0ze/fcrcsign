#pragma once
#include <string_view>
#include <boost/crc.hpp>

namespace util {
// default hasher type for FileCrcSignatureCreator
struct Crc32Hasher {
  // hash_type must be defined
  using hash_type = decltype(std::declval<boost::crc_32_type>().checksum());
  // member function hash with signature like this must be defined
  inline hash_type hash(std::string_view buf)
  {
    boost::crc_32_type result;
    result.process_bytes(buf.data(), buf.size());
    return result.checksum();
  }
};


}
